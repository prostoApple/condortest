//
//  vcList.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "vcList.h"
#import "Entity.h"
#import "EntityCell.h"
#import "Storage.h"
#import "DataTransferManager.h"
#import "LocalStorage.h"
#import "vcEntityDetail.h"

@interface vcList () {
    NSArray *entity;
    id<Storage> storage;
    DataTransferManager *dataTransfer;
    BOOL isLoading;
}

@end

@implementation vcList

-(instancetype) init {
    self = [super init];
    if (self) {
        dataTransfer = [[DataTransferManager alloc] init];
        storage = [LocalStorage shared];
        isLoading = false;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    if (storage.allEntities.count > 0) {
        entity = storage.allEntities;
    } else {
        [dataTransfer loadEntity];
    }
    
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTable) name:@"entitiesUpdates" object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark data

-(void) updateTable {
    entity = storage.allEntities;
    [self.tableView reloadData];
    isLoading = false;
}

#pragma mark SetupUI

-(void) setupUI {
    self.navigationItem.title = @"All items";
    [self setupTableView];
}

-(void) setupTableView {
    [self.tableView registerClass:[EntityCell class] forCellReuseIdentifier:@"entityCell"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [entity count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EntityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"entityCell" forIndexPath:indexPath];
    [cell setupWith:entity[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"EntityDetail" bundle:nil];
    vcEntityDetail *vc = (vcEntityDetail *)[sb instantiateViewControllerWithIdentifier:@"EntityDetail"];
    [vc setEntity:entity[indexPath.row]];
    [self.navigationController pushViewController:vc animated:true];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 92;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;

    if (maximumOffset - currentOffset <= 184) {
        if (!isLoading) {
            [dataTransfer loadEntity];
            isLoading = true;
        }
    }
}

@end
