//
//  EntityCell.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Entity;

@interface EntityCell : UITableViewCell

-(void)setupWith:(Entity *)entity;

@end
