//
//  EntityCell.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "EntityCell.h"
#import "Entity.h"
#import "LoadableImageView.h"

@interface EntityCell()

@property (nonatomic) UILabel *lblCell;
@property (nonatomic) LoadableImageView *ivCell;

@end

@implementation EntityCell

-(void) layoutSubviews {
    [super layoutSubviews];
    [self.contentView addSubview:self.ivCell];
    [self.contentView addSubview:self.lblCell];
}

-(void)setupWith:(Entity *)entity {
    self.lblCell.text = entity.name;
    self.ivCell.imageSource = entity.image;
}

-(UILabel *) lblCell {
    if (!_lblCell) {
        _lblCell = [[UILabel alloc] initWithFrame:CGRectMake(110, (self.frame.size.height - 30) / 2, self.frame.size.width - 120, 30)];
    }
    return _lblCell;
}

-(UIImageView *) ivCell {
    if (!_ivCell) {
        _ivCell = [[LoadableImageView alloc] init];
        _ivCell.frame = CGRectMake(10, 10, 100, self.frame.size.height - 20);
    }
    return _ivCell;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
