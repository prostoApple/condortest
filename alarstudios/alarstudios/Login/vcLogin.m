//
//  vcLogin.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "vcLogin.h"
#import "User.h"
#import "DataTransferManager.h"
#import "UIViewController+Alert.h"
#import "AppDelegate.h"

@interface vcLogin ()
@property (weak, nonatomic) IBOutlet UITextField *inpLogin;
@property (weak, nonatomic) IBOutlet UITextField *inpPass;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) User *tmpUser;

@end

@implementation vcLogin

#pragma mark Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    self.tmpUser = [[User alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark setup UI

-(void) setupUI {
    self.inpLogin.delegate = self;
    self.inpPass.delegate = self;
}

#pragma mark Actions

- (IBAction)login:(UIButton *)sender {
    [self.view endEditing:true];
    [self loginRequest];
}

-(void)loginRequest {
    if (self.tmpUser.isAuthDataComplete) {
        DataTransferManager *manager = [[DataTransferManager alloc] init];
        [manager login:self.tmpUser callback:^(BOOL isSuccess, NSString *message) {
            if (isSuccess) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    [app changeVC];
                });
            } else {
                [self showAlertWith:@"Warning" message: message];
            }
        }];
    } else {
        [self showAlertWith:@"Warning" message:@"All fields are required"];
    }
}

#pragma mark TextField delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.text.length < 3) {
        return false;
    }
    
    if (textField == self.inpLogin) {
        [self.inpPass becomeFirstResponder];
    } else {
        [self.view endEditing:true];
        [self loginRequest];
    }
    
    return true;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length >= 3) {
        if (textField == self.inpLogin) {
            self.tmpUser.login = textField.text;
        } else {
            self.tmpUser.pass = textField.text;
        }
    }
}

@end
