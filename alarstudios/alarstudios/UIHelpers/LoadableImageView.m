//
//  LoadableImageView.m
//  alarstudios
//
//  Created by prostoApple on 30/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "LoadableImageView.h"
#import "ImageLoader.h"

@interface LoadableImageView()

@property (nonatomic, strong) UIActivityIndicatorView *progress;

@end


@implementation LoadableImageView

-(instancetype)init {
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onImageDownloaded:) name:@"imageLoaded" object:nil];
    }
    return self;
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setImageSource:(NSString *)imageSource {
    _imageSource = imageSource;
    NSString *path = [self pathForImage:imageSource];
    UIImage *img = [UIImage imageWithContentsOfFile:path];
    if (img == nil) {
        [self showProgress];
        [[ImageLoader shared] loadImage:imageSource];
    } else {
        self.image = img;
    }
    
}

-(UIActivityIndicatorView *) progress {
    if (!_progress) {
        _progress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _progress.color = [UIColor redColor];
        _progress.center = self.center;
        [_progress setHidesWhenStopped:true];
    }
    return _progress;
}

- (void)onImageDownloaded:(NSNotification *)notification
{
    NSString *localFilename = notification.userInfo[@"localFilename"];
    if ([localFilename isEqualToString:[self pathForImage:self.imageSource]]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSLog(@"FILENAME %@", localFilename);
            UIImage *image = [UIImage imageWithContentsOfFile:localFilename];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.image = image;
                self.contentMode = UIViewContentModeScaleAspectFill;
                [self hideProrgress];
            });
            
        });
    }
}

-(void) showProgress {
    [self addSubview:self.progress];
    [self.progress startAnimating];
}

-(void) hideProrgress {
    [self.progress stopAnimating];
}

-(NSString *)pathForImage:(NSString *)name {
    NSArray *components = [name componentsSeparatedByString:@"/"];
    NSString *imgName = components.lastObject;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return [basePath stringByAppendingPathComponent:imgName];
}


@end
