//
//  LoadableImageView.h
//  alarstudios
//
//  Created by prostoApple on 30/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadableImageView : UIImageView

@property (nonatomic, strong) NSString *imageSource;

-(void) showProgress;

@end
