//
//  UIViewController+Alert.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "UIViewController+Alert.h"

@implementation UIViewController (Alert)

-(void) showAlertWith:(NSString *) title message:(NSString *) message  {
    
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:title message:message preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [ac addAction:ok];
    [self presentViewController:ac animated:true completion:nil];
    
}

@end
