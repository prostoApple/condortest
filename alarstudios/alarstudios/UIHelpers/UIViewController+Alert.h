//
//  UIViewController+Alert.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Alert)

-(void) showAlertWith:(NSString *) title message:(NSString *) message;

@end
