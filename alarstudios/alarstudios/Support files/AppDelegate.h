//
//  AppDelegate.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
-(void) changeVC;

@end

