//
//  AppDelegate.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "AppDelegate.h"
#import "vcList.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    return YES;
}


-(void) changeVC {
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[[vcList alloc] init]];
    [self.window makeKeyAndVisible];
}


@end
