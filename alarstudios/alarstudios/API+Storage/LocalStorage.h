//
//  LocalStorage.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Storage.h"
@class User;

@interface LocalStorage : NSObject <Storage>

+(instancetype) shared;
-(User *) currentUser;
-(void)saveUser: (User *)newUser;
-(void)saveImage:(UIImage *)image with:(NSString *)name;
-(UIImage *)imageForName:(NSString *)name;
-(BOOL)isFileExist:(NSString *)path;
-(NSString *)pathForImage:(NSString *)name;

@end
