//
//  HTTPService.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPService : NSObject

-(instancetype)init;

-(void)apiGETRequest:(NSURLRequest *)request completion:(void(^)(BOOL isSuccess, NSDictionary *responseDic)) completeBlock;
-(void)getRequest:(NSURLRequest *)request completion:(void(^)(BOOL isSuccess, NSData *responseData)) completeBlock;

@end
