//
//  Storage.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@protocol Storage <NSObject>

-(User *) currentUser;
-(void)saveUser: (User *)newUser;
-(NSArray *)allEntities;
-(void)appendEntities: (NSArray *) newArr;
-(void)saveImage:(UIImage *)image with:(NSString *)name;
-(UIImage *)imageForName:(NSString *)name;

@end
