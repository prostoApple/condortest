//
//  ImageLoader.h
//  alarstudios
//
//  Created by prostoApple on 30/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageLoader : NSObject

+(instancetype)shared;
-(void)loadImage:(NSString *)source;

@end
