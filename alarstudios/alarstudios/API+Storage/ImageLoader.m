//
//  ImageLoader.m
//  alarstudios
//
//  Created by prostoApple on 30/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "ImageLoader.h"
#import "DataTransferManager.h"
#import "Storage.h"
#import "LocalStorage.h"

@interface ImageLoader() {
    NSMutableArray *stack;
    BOOL isDownloading;
}

@end

@implementation ImageLoader

+(instancetype)shared {
    static ImageLoader *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] initPrivate];
    });
    return shared;
}

-(instancetype) initPrivate {
    self = [super init];
    if (self) {
        stack = [NSMutableArray array];
    }
    return self;
}

-(void)loadImage:(NSString *)source {
    if (![stack containsObject:source]) {
        [stack addObject:source];
        if (!isDownloading) {
            [self loadFromStack];
        }
    }
}

-(void)loadFromStack {
    if ([stack count] != 0 && !isDownloading) {
        isDownloading = true;
        DataTransferManager *dataTransfer = [[DataTransferManager alloc] init];

        NSString *source = stack.firstObject;
        [dataTransfer loadImage:source callback:^(BOOL isFinished) {
            isDownloading = false;
            [stack removeObjectAtIndex:0];
            [self loadFromStack];
        }];
    }
}

@end
