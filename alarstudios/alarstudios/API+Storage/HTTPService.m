//
//  HTTPService.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "HTTPService.h"

@implementation HTTPService {
    NSURLSession *currentSession;
}

-(instancetype)init {
    self = [super init];
    if (self) {
        currentSession = [NSURLSession sharedSession];
    }
    return self;
}

-(void)apiGETRequest:(NSURLRequest *)request completion:(void(^)(BOOL isSuccess, NSDictionary *responseDic)) completeBlock {
    if (request) {
        NSURLSessionDataTask* task = [currentSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable __unused response, NSError * _Nullable __unused error) {
            NSHTTPURLResponse *currentHttpResponseStatus = (NSHTTPURLResponse *)response;
            if (currentHttpResponseStatus.statusCode == 200) {
                if (data) {
                    NSError *err;
                    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:NSJSONReadingMutableContainers
                                                                            error:&err];
                    
                    completeBlock(true, response);
                    return;
                }
            }
            completeBlock(false, nil);
        }];
        [task resume];
    } else {
        completeBlock(false, nil);
    }
}

-(void)getRequest:(NSURLRequest *)request completion:(void(^)(BOOL isSuccess, NSData *responseData)) completeBlock {
    if (request) {
        NSURLSessionDataTask* task = [currentSession dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable __unused response, NSError * _Nullable __unused error) {
            NSHTTPURLResponse *currentHttpResponseStatus = (NSHTTPURLResponse *)response;
            if (currentHttpResponseStatus.statusCode == 200) {
                if (data) {
                    completeBlock(true, data);
                    return;
                }
            }
            completeBlock(false, nil);
        }];
        [task resume];
    } else {
        completeBlock(false, nil);
    }
}

@end
