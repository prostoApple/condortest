//
//  LocalStorage.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "LocalStorage.h"
#import "User.h"
#import "Entity.h"


@interface LocalStorage() {
    User *user;
    NSMutableArray *entities;
}

@end

@implementation LocalStorage

+(instancetype)shared {
    static LocalStorage *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] initPrivate];
    });
    
    return shared;
}

-(instancetype) initPrivate {
    self = [super init];
    if (self) {
        entities = [NSMutableArray array];
    }
    return self;
}

-(User *)currentUser {
    return user;
}

-(void)saveUser: (User *)newUser {
    user = newUser;
}

-(void)appendEntity: (Entity *)new {
    [entities addObject:new];
}

-(void)appendEntities: (NSArray *) newArr {
    [entities addObjectsFromArray:newArr];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"entitiesUpdates" object:nil];
}

-(NSArray *)allEntities {
    return entities;
}

-(void)saveImage:(UIImage *)image with:(NSString *)name {
    NSData *binaryImageData = UIImagePNGRepresentation(image);
    NSString *path = [self pathForImage:name];
    if (![self isFileExist:path]) {
        [binaryImageData writeToFile:path atomically:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"imageLoaded" object:nil userInfo:@{@"localFilename":path}];
    }
}

-(UIImage *)imageForName:(NSString *)name {
    NSString *path = [self pathForImage:name];
    if ([self isFileExist:path]) {
        return [UIImage imageWithContentsOfFile:path];
    } else {
        return nil;
    }
}

-(BOOL)isFileExist:(NSString *)path {
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

-(NSString *)pathForImage:(NSString *)name {
    NSArray *components = [name componentsSeparatedByString:@"/"];
    NSString *imgName = components.lastObject;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return [basePath stringByAppendingPathComponent:imgName];
}

@end
