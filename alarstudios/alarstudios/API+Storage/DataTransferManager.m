//
//  DataTransferManager.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "DataTransferManager.h"
#import "User.h"
#import "HTTPService.h"
#import "LocalStorage.h"
#import "Storage.h"
#import "Entity.h"

#define BASE_URL @"http://condor.alarstudios.com/test/"

@interface DataTransferManager() {
    id<Storage> storage;
    HTTPService *apiService;
    NSInteger lastEntityPage;
    BOOL isCanLoadMore;
}

@end

@implementation DataTransferManager

-(instancetype) init {
    self = [super init];
    if (self) {
        storage = [LocalStorage shared];
        apiService = [[HTTPService alloc] init];
        lastEntityPage = 1;
        isCanLoadMore = true;
    }
    return self;
}

-(void)login:(User *)user callback:(void(^)(BOOL isSuccess, NSString *message))completion {
    
    NSString *param = [NSString stringWithFormat:@"auth.cgi?username=%@&password=%@", user.login, user.pass];
    NSString *url = [BASE_URL stringByAppendingString:param];
    
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [apiService apiGETRequest:req completion:^(BOOL isSuccess, NSDictionary *response) {
        if (isSuccess) {
            if (response[@"status"] != nil && [response[@"status"] isEqualToString:@"ok"]) {
                completion(true, @"Success");
                [storage saveUser:[User init:user.login pass:user.pass key:response[@"code"]]];
            } else {
                completion(false, @"логин/пароль неправильные");
            }
        } else {
            completion(false, @"Newtwork error");
        }
    }];
}

-(void)loadEntity {
    NSString *param = [NSString stringWithFormat:@"data.cgi?code=%@&p=%ld", storage.currentUser.key, (long)lastEntityPage + 1];
    NSString *urlString = [BASE_URL stringByAppendingString:param];
    
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    if (isCanLoadMore) {
        
        [apiService apiGETRequest:req completion:^(BOOL isSuccess, NSDictionary *response) {
            if (isSuccess) {
                if (response[@"status"] != nil && [response[@"status"] isEqualToString:@"ok"]) {
                    lastEntityPage = [response[@"page"] integerValue];
                    [self parseEntities:response[@"data"]];
                    isCanLoadMore = true;
                } else {
                    isCanLoadMore = false;
                }
            }
        }];
    }
}

-(void)loadImage:(NSString *)sourcePath callback:(void(^)(BOOL isFinished))completion {
    NSURL *url = [NSURL URLWithString:sourcePath];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:url];

    [apiService getRequest:req completion:^(BOOL isSuccess, NSData *responseData) {
        if (isSuccess) {
            UIImage *img = [UIImage imageWithData:responseData];
            if (img != nil) {
                [storage saveImage:img with:sourcePath];
            }
        }
        completion(true);
    }];
}

-(void)parseEntities: (NSArray *)entitiesArr {
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *entityJson in entitiesArr) {
        [arr addObject:[Entity initWithJSON:entityJson]];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [storage appendEntities:arr];
    });
}

@end
