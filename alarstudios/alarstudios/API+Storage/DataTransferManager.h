//
//  DataTransferManager.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class User;

@interface DataTransferManager : NSObject

-(void)login:(User *)user callback:(void(^)(BOOL isSuccess, NSString *message))completion;
-(void)loadEntity;
-(void)loadImage:(NSString *)sourcePath callback:(void(^)(BOOL isFinished))completion;

@end
