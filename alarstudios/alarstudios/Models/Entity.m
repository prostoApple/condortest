//
//  Entity.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "Entity.h"

@implementation Entity

+(instancetype) initWithJSON:(NSDictionary *)json {
    Entity *new = [[Entity alloc] init];
    new.id = json[@"id"];
    new.name = json[@"name"];
    new.country = json[@"country"];
    new.lat = @([json[@"lat"] floatValue]);
    new.lon = @([json[@"lon"] floatValue] );
    new.image = @"http://spbstory.ru/wp-content/uploads/2014/08/qybl2cjytkk-1024x727.jpg";

    return new;
}

@end
