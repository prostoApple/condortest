//
//  User.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSString *login;
@property (nonatomic, strong) NSString *pass;

+(instancetype)init:(NSString *)log pass:(NSString *)pswd key:(NSString *)key;
-(NSString *)key;
-(BOOL)isAuthDataComplete;

@end
