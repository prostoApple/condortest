//
//  User.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "User.h"

#define CURRENT_USER_KEY @"CURRENT_USER_KEY"

@interface User()

@property (nonatomic, strong) NSString *key;

@end

@implementation User

+(instancetype)init:(NSString *)log pass:(NSString *)pswd key:(NSString *)key {
    User *user = [[User alloc] init];
    if (user) {
        user.login = log;
        user.pass = pswd;
        user.key = key;
    }
    return user;
}

-(void)setKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] setValue:key forKey:CURRENT_USER_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"Key saved %@",key);
}

-(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] valueForKey:CURRENT_USER_KEY];
}

-(BOOL)isAuthDataComplete {
    return (self.login.length != 0 && self.pass.length != 0) ? true : false;
}

@end
