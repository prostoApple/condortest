//
//  MapAnnotation.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "MapAnnotation.h"


@implementation MapAnnotation
@synthesize coordinate, title;

-(instancetype) initWithTitle:(NSString *)name andCoordinate:(CLLocationCoordinate2D) coord {
    self = [self init];
    if (self) {
        coordinate = coord;
        title = name;
    }
    return self;
}

@end
