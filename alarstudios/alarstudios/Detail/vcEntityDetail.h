//
//  vcEntityDetail.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@class Entity;

@interface vcEntityDetail : UIViewController

-(void)setEntity:(Entity *)ent;

@end
