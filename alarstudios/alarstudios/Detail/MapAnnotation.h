//
//  MapAnnotation.h
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnnotation : NSObject <MKAnnotation> {
    CLLocationCoordinate2D coordinate;
    NSString *title;
}

- (instancetype) initWithTitle:(NSString *) title andCoordinate:(CLLocationCoordinate2D) coordinate;

@end
