//
//  vcEntityDetail.m
//  alarstudios
//
//  Created by prostoApple on 25/11/2017.
//  Copyright © 2017 ad. All rights reserved.
//

#import "vcEntityDetail.h"
#import "Entity.h"
#import "MapAnnotation.h"

@interface vcEntityDetail () {
    CLLocation *location;
    Entity *entity;
}

@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UILabel *lblIdentificator;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblCountry;

@end

@implementation vcEntityDetail

- (instancetype)init:(Entity *) entity
{
    self = [super init];
    if (self) {
        location = [[CLLocation alloc] initWithLatitude:[entity.lat floatValue] longitude:[entity.lon floatValue]];
    }
    return self;
}

-(void)setEntity:(Entity *)ent {
    entity = ent;
    location = [[CLLocation alloc] initWithLatitude:[entity.lat floatValue] longitude:[entity.lon floatValue]];
}

#pragma mark Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupUI];
    [self setupMap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark SetupUI

-(void)setupUI {
    self.navigationItem.title = entity.name;
    self.lblName.text = entity.name;
    self.lblCountry.text = entity.country;
    self.lblIdentificator.text = entity.id;
}

-(void)setupMap {
    MKCoordinateRegion mapRegion;
    mapRegion.center.longitude = location.coordinate.longitude;
    mapRegion.center.latitude = location.coordinate.latitude;
    mapRegion.span.latitudeDelta = 0.03;
    mapRegion.span.longitudeDelta = 0.03;
    [self.map setRegion:mapRegion animated: YES];
    MapAnnotation *annotation = [[MapAnnotation alloc] initWithTitle:entity.name andCoordinate:location.coordinate];
    [self.map addAnnotation:annotation];
}

@end
